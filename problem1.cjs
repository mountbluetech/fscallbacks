const fs = require("fs");

let content = { a: "first", b: "next", c: "last" };

//Used fs.access to check if the directory is present. if not mkdir is called to create folder
function makeDirectory(fullDir, callback) {
  fs.access(fullDir, (error) => {
    if (error) {
      fs.mkdir(fullDir, (error) => {
        if (error) {
          console.log("Cannot create Folder");
        } else {
          console.log("Folder created successfully");
        }
      });
      callback(fullDir);
    } else {
      console.log("File already exists");
      callback(fullDir);
    }
  });
}

//Used to create a n number of JSON files
function createFilesInDir(fullDir, noOfFiles, callback) {
  let count = 0;
  for (let i = 1; i <= noOfFiles; i++) {
    fs.writeFile(
      `${fullDir}/file${i}.JSON`,
      JSON.stringify(content),
      { flag: "a+" },
      (error) => {
        if (error) {
          console.log("Cannot Create Files");
        } else {
          console.log(`Created File${i} successfully`);
          count++;
          if (count === noOfFiles) {
            callback(fullDir);
          }
        }
      }
    );
  }
}

//Delete n number of files in the given directory
function deleteFilesInDir(directory, filesCount) {
  let count = 0;
  fs.readdir(directory, "utf-8", (error, files) => {
    if (error) {
      console.log("cannot read Directory");
    } else {
      files.forEach((file) => {
        count++;
        if (count > filesCount) return;
        else {
          fs.unlink(`${directory}/${file}`, (error) => {
            if (error) {
              console.log(`not able to delete the file ${file}`);
            } else {
              console.log(`deleting the file ${file}`);
            }
          });
        }
      });
    }
    // console.log(files);
  });
}

module.exports = { makeDirectory, createFilesInDir, deleteFilesInDir };
