let fs = require("fs");

function readFile(fullPath, callback) {
  fs.readFile(fullPath, (error, content) => {
    if (error) {
      console.log(error);
    } else {
      // console.log(content.toString());
      callback(content.toString());
    }
  });
}

function appendToFile(data, intoFile) {
  fs.appendFile(intoFile, data, (error) => {
    if (error) console.log(error);
    else {
      console.log(`Added ${data} to ${intoFile}`);
    }
  });
}
function writeToFile(fileName, content, callback) {
  // console.log("\n"+fileName);
  // console.log(content);
  fs.writeFile(fileName, content, (error) => {
    console.log(
      `${
        error
          ? "not able to create file"
          : "created " + fileName + " successfully"
      }`
    );
  });
  callback(content);
}

function deleteFiles(namesFile, callback) {
  fs.readFile("./fileNames.txt", "utf-8", (error, data) => {
    if (error) console.log(error);

    const fileNamesToDelete = data
      .split("\n")
      .filter((fileName) => fileName !== "");

    fileNamesToDelete.forEach((fileName) => {
      fs.unlink(fileName, (error) => {
        if (error) {
          console.log(error);
          return;
        } else {
          console.log(`File '${fileName}' deleted successfully.`);
        }
      });
    });
  });
  callback();
}

module.exports = { readFile, writeToFile, appendToFile, deleteFiles };
