let {
  makeDirectory,
  createFilesInDir,
  deleteFilesInDir,
} = require("../problem1.cjs");
let path = require("path");
let folderName = "JSON test folder";
let fullDir = path.join(__dirname, `../${folderName}`);
let generateFilesCount = 5;
let deleteFilesCount = 5;

makeDirectory(fullDir, (dir) => {
  return createFilesInDir(dir, generateFilesCount, (dir) => {
    deleteFilesInDir(dir, deleteFilesCount);
  });
});
