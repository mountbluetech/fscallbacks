let path = require("path");
let relativeFilePath = path.join(__dirname, "../data/lipsum.txt");
let {
  readFile,
  appendToFile,
  writeToFile,
  deleteFiles,
} = require("../problem2.cjs");
let uppercaseFileName = "uppercase.txt";
let lowercaseFileName = "lowercase.txt";
let sortedTextFileName = "sorted.txt";
let storedNames = "fileNames.txt";

readFile(relativeFilePath, (content) => {
  let upperCaseContent = content.toUpperCase();
  writeToFile(uppercaseFileName, upperCaseContent, (upperCaseContent) => {
    appendToFile(uppercaseFileName + "\n", storedNames);
    let lowercaseContent = upperCaseContent.toLowerCase();
    let sentences = lowercaseContent.split(". ");
    // console.log(sentences);
    writeToFile(lowercaseFileName, sentences.join("\n"), () => {
      appendToFile(lowercaseFileName + "\n", storedNames);
      readFile(lowercaseFileName, (content) => {
        let sortedText = content.split("\n").sort().join("\n");
        writeToFile(sortedTextFileName, sortedText, () => {
          appendToFile(sortedTextFileName + "\n", storedNames);
          deleteFiles(storedNames, () => {});
        });
      });
    });
  });
});
